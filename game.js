let randomNumber = Math.floor(Math.random() * 100) + 1;

function checkGuess() {
    const userGuess = parseInt(document.getElementById('guess').value);
    const message = document.getElementById('message');

    if (isNaN(userGuess) || userGuess < 1 || userGuess > 100) {
        message.textContent = "Please enter a valid number between 1 and 100.";
    } else if (userGuess === randomNumber) {
        message.textContent = "Congratulations! You guessed the right number!";
        resetGame();
    } else if (userGuess > randomNumber) {
        message.textContent = "Too high! Try again.";
    } else if (userGuess < randomNumber) {
        message.textContent = "Too low! Try again.";
    }
}

function resetGame() {
    randomNumber = Math.floor(Math.random() * 100) + 1;
    document.getElementById('guess').value = '';
    setTimeout(() => {
        document.getElementById('message').textContent = '';
    }, 3000);
}
